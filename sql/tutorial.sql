
DROP TABLE Theaters;
DROP TABLE Schedules;
DROP TABLE Plays;
DROP DATABASE mt_dev;

CREATE DATABASE mt_dev;

/*----CREATE TABLE----*/

USE mt_dev;
CREATE TABLE Theaters(
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50),
    city VARCHAR(50),
    director_name VARCHAR(50)
);

CREATE TABLE Schedules(
    id int PRIMARY KEY AUTO_INCREMENT,
    theater_id int,
    play_id int,
    date_time DATE,
    ticket_price int
);

CREATE TABLE Plays(
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50),
    writer_name VARCHAR(50),
    director_name VARCHAR(50),
    description VARCHAR(100)
);


/*----CONSTRAINTS----*/

ALTER TABLE Schedules
ADD FOREIGN KEY (theater_id) REFERENCES Theaters(id) ON DELETE CASCADE;

ALTER TABLE Schedules
ADD FOREIGN KEY (play_id) REFERENCES Plays(id) ON DELETE CASCADE;


/*----INSERT----*/

INSERT INTO Theaters(name,city, director_name) VALUES("Sydney Opera House", "Sydney", "Sergiu Nicolaescu");
INSERT INTO Theaters(name,city, director_name) VALUES("Metropolitan Opera House", "New York", "Charlie Chaplin");

INSERT INTO Plays(name, writer_name, director_name, description) VALUES("Phantom of the Opera","Andrew Lloyd Webber","John Doe", "A chilling story about a girl, a boy, and a phantom");
INSERT INTO Plays(name, writer_name, director_name, description) VALUES("Romeo and Juliet","William Shakespear","Jane Doe", "A lovely story about a girl, a boy, and a city");
INSERT INTO Plays(name, writer_name, director_name, description) VALUES("O scrisoare pierduta","Ion Luca Caragiale","Ion Doe", "A funny story about a girl, a boy, and a letter");
INSERT INTO Plays(name, writer_name, director_name, description) VALUES("Sinbad's Adventures","Bill Willingham","Jon Deo", "A daring story about a girl, a boy, and a treasure");

INSERT INTO Schedules(theater_id, play_id, date_time, ticket_price) VALUES(1,1,STR_TO_DATE('2020-07-19','%Y-%m-%d'),25);
INSERT INTO Schedules(theater_id, play_id, date_time, ticket_price) VALUES(2,1,STR_TO_DATE('2021-07-19','%Y-%m-%d'),50);
INSERT INTO Schedules(theater_id, play_id, date_time, ticket_price) VALUES(2,2,STR_TO_DATE('2022-07-19','%Y-%m-%d'),75);

INSERT INTO Theaters(name,city, director_name) VALUES("Teatrul National Caracal", "Caracal", "Birja Birjarul");


/*----INTEROGATION----*/

/*plays that are scheduled in first theater (id = 1) */
SELECT p.name 
FROM Plays p, Theaters t, Schedules s
WHERE p.id = s.play_id 
  AND t.id = s.theater_id
  AND t.id = 1;

/* number of distinct plays that were played in each theater */
SELECT t.id, COUNT(DISTINCT(p.id)) AS 'Number of Distinct plays'
FROM Theaters t, Plays p, Schedules s 
WHERE p.id = s.play_id 
  AND t.id = s.theater_id
GROUP BY t.id;

/*total price for each play*/
SELECT p.id, SUM(s.ticket_price) AS 'Sum of tickets'
FROM Theaters t, Plays p, Schedules s 
WHERE p.id = s.play_id 
  AND t.id = s.theater_id
GROUP BY p.id;


/*----UPDATE----*/

/*Update ticket_price for the cheapest play schedule by adding it’s id*/
SELECT @price := ticket_price
FROM Schedules
ORDER BY ticket_price DESC;

UPDATE Schedules
SET ticket_price = ticket_price + id 
WHERE ticket_price = @price;

SELECT * from Schedules;


/*----DELETE----*/

DELETE FROM Schedules
WHERE id = 1;

DELETE FROM Theaters
WHERE id = 1;

DELETE FROM Plays
WHERE id = 1;

SELECT * FROM Schedules;
SELECT * FROM Theaters;
SELECT * FROM Plays;
COMMIT;